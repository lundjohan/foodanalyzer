# -*- coding: utf-8 -*-
"""
Created on Sun May 22 13:17:29 2016

@author: Johan
"""
from datetime import timedelta
import collections
import copy  
from excel_importer import parseToDays
def printDays(days):
    for day in days:
        print (str(day.date),' => ',day.score)

def getDaysBeforeDaysWithPoints(days, minPoint, maxPoint):
    daysWithPoints = getDatesForDaysWithPoints(days, minPoint, maxPoint)
    daysBefore = getDatesOfDaysBefore(daysWithPoints)
    return getDaysWithDates(days, daysBefore)
 
#N.B. minPoint and maxPoint INCLUDED in calc 
def getDaysByPoints(days, minPoint, maxPoint):
    filteredDays = []
    for day in days:
        if day.score >= minPoint and day.score <= maxPoint:
            filteredDays.append(day)
    return filteredDays 
def getDatesForDaysWithPoints(days, minPoint, maxPoint):
    dates = []
    for day in days:
        if day.score >= minPoint and day.score <= maxPoint:
            dates.append(day.date)
    return dates            
    
def getDatesOfDaysBefore(dates):
    datesOfDaysBefore = []    
    for date in dates:
        dateBefore = date - timedelta(days=1)
        datesOfDaysBefore.append (dateBefore)
    return datesOfDaysBefore
    
def getDaysWithDates(days, dates):
    resDays = []    
    for day in days:
        if day.date in dates:
            resDays.append(day)
    return resDays
#necessary that days are ordered according to date    
def getDaysBefore(days):
  return days[0:-1]

#changes days array so that points are based on the following day
def moveUpFollowingDayPoints(days):
    #removeDaysWithoutTags(days)  har lagt referens till denna högre upp i kod.
    daysBefore = copy.deepcopy(days)[:-1]
    for i in range(len(daysBefore)):
        daysBefore[i].score = days[i+1].score
    return daysBefore   
#these days are not supposed to be calculated upon
#for code understanding see => http://stackoverflow.com/questions/1207406/remove-items-from-a-list-while-iterating-in-python    
def removeDaysWithoutTags(days):
    days[:] = [day for day in days if len(day.tags())>0]    
    return days
            
def printDaysBeforeBestDays():
    allDays = parseToDays('pesc.xlsx')
    daysBefore = getDaysBeforeDaysWithPoints(allDays,4.5,5.0)
    printDays(daysBefore)
                
def printTagsInDaysBeforeDaysWithPoints(days, nr_of_most_common, minScore, maxScore):    
    allTags = []    
    daysBefore = getDaysBeforeDaysWithPoints(days,minScore,maxScore)           
    for day in daysBefore:
        allTags.extend(day.tags)
    c = collections.Counter(allTags).most_common(nr_of_most_common)
    print ("Antal Dagar: "+str(len(daysBefore))+'\n')
    for t in c:
        print (t[0].encode('utf-8') +'=>'+ str(t[1])) 

      
def printToFileDayBefore(fileHandler, allDays, nr_of_most_common, minScore, maxScore):   
    allTagNames = []    
    filteredDays = getDaysBeforeDaysWithPoints(allDays, minScore,maxScore)           
    for day in filteredDays:
        for tag in day.tags():
            try:            
                i = int(tag.portion) 
            except Exception:
                i=1         #portion
            while i>=1:
                allTagNames.append(tag.name) 
                i=i-1
    c = collections.Counter(allTagNames).most_common(nr_of_most_common)
    fileHandler.write("Antal Dagar: "+str(len(filteredDays))+'\n')
    for t in c:
        fileHandler.write( str(t[0]) +'=>'+ str(t[1])+'\n')
        
def printToFileSameDay(fileHandler, allDays, nr_of_most_common, minScore, maxScore):   
    allTagNames = []    
    filteredDays = getDaysByPoints(allDays,minScore,maxScore)           
    for day in filteredDays:
        for tag in day.tags():
            try:            
                i = int(tag.portion) 
            except Exception:
                i=1         #portion
            while i>0:
                allTagNames.append(tag.name)    
                i-=1
    c = collections.Counter(allTagNames).most_common(nr_of_most_common)
    fileHandler.write("Antal Dagar: "+str(len(filteredDays))+'\n')
    for t in c:
        fileHandler.write( str(t[0]) +'=>'+ str(t[1])+'\n')
        
#Nedan post-objects
def bestTagsForBreakfast(fileHandler, nr_of_most_common, minScore, maxScore):
    f = fileHandler    
    allTagStr = []    
    #f = open('BestTagsForBreakfast.txt','w')    
    allDays = parseToDays('pesc.xlsx')    
    dates = getDatesForDaysWithPoints(allDays, minScore, maxScore)
    days = getDaysWithDates(allDays, dates)
    for day in days:
        print (day.date)
        if day.meals:  
            if day.meals[0].tags:
                for tag in day.meals[0].tags:  
                    allTagStr.append(tag.name) 
    c = collections.Counter(allTagStr).most_common(nr_of_most_common)
    f.write("Antal Dagar: "+str(len(days))+'\n')
    for t in c:
        f.write( t[0].encode('utf-8') +'=>'+ str(t[1])+'\n')    
   # f.close()

    
def bestSizeForBreakfast(fileHandler, nr_of_most_common, minScore, maxScore):
    f = fileHandler    
    allSizes = []       
    allDays = parseToDays('pesc.xlsx')    
    dates = getDatesForDaysWithPoints(allDays, minScore, maxScore)
    days = getDaysWithDates(allDays, dates)
    for day in days:
        print (day.date)
        if day.meals:
            portions = day.meals[0].portions
            if portions:
                print (portions)
                allSizes.append(portions)
    c = collections.Counter(allSizes).most_common(nr_of_most_common)
    f.write("Antal Dagar: "+str(len(days))+'\n')
    for t in c:
        f.write(str(t[1]))
        #f.write( (t[0],'=>' ,str(t[1]),'\n'))
#simpler methtod than getRecommendations, Based on whichever days that is in argument 
def getRec(f, allDays): 
    tagsDict = pointsToTags(allDays)   #score still zero, but occurences finished   

        
    f.write('Tags in order, positive score means good'+'\n')   
    writeDictToFile(f, tagsDict)
#    sortedByScore = {}
#    
#    for tagName in tagsDict:#, key=allTags.get(1), reverse=True):       
#        sortedByScore[tagName] = float(tagsDict[tagName][0])/float(tagsDict[tagName][1])
#          
#    
#    for tagName in sorted (sortedByScore, key=sortedByScore.get, reverse=True):
#        #if occurence of tag >=3
#       if tagsDict[tagName][1] >=1:    
#           f.write(tagName + ' => Points: ' + str(sortedByScore[tagName])+' Nr Of Occurences: '+str(tagsDict[tagName][1])+'\n')
#    
#the holy grail of IBS food logarithms. Result (z) Based on day before and same day (x, y). Just nu så är x lika wiegted som y men kan hända att dagen innan är viktigare eller vice versa
def getRecForBothDays(f, allDays): 
    #tagsDict is an a dict with the unique 'input' tagNames in allDays (tags found in meal, exercise, other) 
    #tagsDict structure: {milk: (45,10)} => explanation of tupel: tuple[0]=> accumluated points, tuple[1] nr of occurences for tag
    adjDays = adjustDayScore(allDays)  
    getRec(f, adjDays)
    
def adjustDayScore(allDays):
    #adjust score of day by taking into account the day after, the days are equally weighted    
    for i in range(len(allDays)-1):
        allDays[i].score = (allDays[i].score+allDays[i+1].score)/2
    return allDays#behövs denna return?
#returns Dictionary with structure: {milk: (45,10)} => explanation of tupel: tuple[0]=> accumluated points, tuple[1] nr of occurences for tag    
def pointsToTags(adjDays):
    tagsDict = {}
    for day in adjDays:
        for tag in day.tagsExtended():
            tagsDict[tag.name] = tagsDict.get(tag.name,[0.0,0.0])
            tagsDict[tag.name][0] += day.score*tag.portion      #detta blir galet.
            
            #increment occurences (milk*3)
            tagsDict[tag.name][1] +=tag.portion            
    return tagsDict
#  Down handles meal size, maount of food etc
#
def getWithoutTag(f, allDays):
    #0. Create a dict with key:tagname, value: [accumultatedScoreDaysNotPresent, Occurences of tags not present]    
    withoutTag= dict()    
    
    #1. Add all tags to a set
    allTags = set()
    for d in allDays:
        for tagName in d.tagNames():
            allTags.add(tagName)
            
    #2. which days are tag nonexistent?
    for tagName in allTags:
        for d in allDays:
            if tagName not in d.tagNames():
               withoutTag[tagName] = withoutTag.get(tagName,[0.0,0]) 
               withoutTag[tagName][0] += d.score
               withoutTag[tagName][1] +=1
    writeDictToFile(f, withoutTag)
   
def writeDictToFile(f, diction):
    averageDict = {}    
    for tagName in diction:
        #accumulated score/occurences        
        average = float(diction[tagName][0])/float(diction[tagName][1])  
        #this is horribly ugly => simplify!
        averageDict[tagName] = averageDict.get(tagName,[average, diction[tagName][1]])
    for  tagName in sorted (averageDict, key=averageDict.get, reverse=True):            
        if (averageDict[tagName][1]>=3):        
            f.write(str(tagName)+  ' =>  Average Score: ')
            f.write("%.2f" % averageDict[tagName][0])
            f.write(' Occurrences: '+str(averageDict[tagName][1])+'\n\n')
    
def getAmountPortionsStat(f, allDays):
    #portionsDay: dictionary like this: {portionsForDay: [portionDay1 + portionDay2...,occurrences]}    
    portionsDay = {}    
    for d in allDays:
        portions = 0        
        for meal in d.meals:           
            portion = int(round(float(meal.portions)))            
            portions += portion               
        portionsDay[portions] = portionsDay.get(portions,[0,0])
        portionsDay[portions][0] += d.score
        
        #increment occurences:
        portionsDay[portions][1] += 1
                
    averageDict = {}    
    for portions in portionsDay:
        #accumulated score/occurences        
        average = float(portionsDay[portions][0])/float(portionsDay[portions][1])  
        #this is horribly ugly => simplify!
        averageDict[portions] = averageDict.get(portions,[average, portionsDay[portions][1]])
    for portionAmount in sorted (averageDict, key=averageDict.get, reverse=True):
        if portionAmount >2:             
              f.write(str(portionAmount)+ " portions/day\n" + 'Average Score: '+ str(averageDict[portionAmount][0]) + ' Occurences: '+str(averageDict[portionAmount][1])+'\n\n')
def getMealsStat(f, allDays): 
    nrMealsDays = {}    
    for d in allDays:
        mealsOneDay = len(d.meals) 
        nrMealsDays[mealsOneDay] = nrMealsDays.get(mealsOneDay,[0,0])
        nrMealsDays[mealsOneDay][0] += d.score
        
        #increment occurences:
        nrMealsDays[mealsOneDay][1] += 1
    averageDict = {}    
    for meals in nrMealsDays:
        #accumulated score/occurences        
        averageScore = float(nrMealsDays[meals][0])/float(nrMealsDays[meals][1])  
        #this is horribly ugly => simplify!
        averageDict[meals] = averageDict.get(meals,[averageScore, nrMealsDays[meals][1]])
    for mealNr in sorted (averageDict, key=averageDict.get, reverse=True):
        if mealNr >2:   
            f.write(str(mealNr)+ " meals/day\n" + 'Average Score: '+ str(averageDict[mealNr][0]) + ' Occurences: '+str(averageDict[mealNr][1])+'\n\n')
def timeLastMeal(f, allDays):
    lastMealTimes = {}    
    for day in allDays:
        if len(day.meals)>0:        
            time = day.meals[-1].time
            strTime = time.strftime('%H')
            lastMealTimes[strTime] = lastMealTimes.get(strTime,[0,0])
            lastMealTimes[strTime][0] += day.score
            lastMealTimes[strTime][1] += 1
    averageDict = {}    
    for lastMealTime in lastMealTimes:
        #accumulated score/occurences        
        averageScore = float(lastMealTimes[lastMealTime][0])/float(lastMealTimes[lastMealTime][1])            
        averageDict[lastMealTime] = averageDict.get(lastMealTime,[averageScore, lastMealTimes[lastMealTime][1]])
        
    for lastMealNr in sorted (averageDict, key=averageDict.get, reverse=True):
        if averageDict[lastMealNr][1] >= 3:    
            f.write(lastMealNr+ " is last hour " + 'Average Score: '+ str(averageDict[lastMealNr][0]) + ' Occurences: '+str(averageDict[lastMealNr][1])+'\n\n')

def blueZonesStat(d, allDays):
    return listedTagsByDayScore(d, allDays, 4.8, 5.0)
def goodZonesStat(d, allDays):
    return listedTagsByDayScore(d, allDays, 4.5, 4.7)
def ordinaryZonesStat(d, allDays):
    return listedTagsByDayScore(d, allDays, 3.9, 4.4)
def darkZonesStat(d, allDays):
    return listedTagsByDayScore(d, allDays, 0.0, 3.5)

def listedTagsByDayScore(f, allDays, min_score, max_score):
    tagsGoodScoreDays = []
    protoTagsOtherDays = {} #tagname, nr_of_occurences    
    tagsOtherDays = set()
    existingToPrint = {} #tagName, nrOfGoodDaysItOccurs
    OCCURENCE_LIMIT = 5    
    nonExistingToPrint = {}    #tagName, nrOfGoodDaysItDoesNotOccurs  OBS! SKa bara vara de tagname med occurence >= OCCURENCE_LIMIT för allDays
    for day in allDays:
        if day.score >= min_score and day.score<=max_score:
            tagnames = set()            
            for tag in day.tagsExtended():
                tagnames.add(tag.name)    
            tagsGoodScoreDays.append(tagnames)
        else:
            for tag in day.tagsExtended():
                protoTagsOtherDays[tag.name] = protoTagsOtherDays.get(tag.name,0)
                protoTagsOtherDays[tag.name]+=1
    for tagname in protoTagsOtherDays:
        if protoTagsOtherDays[tagname] >= OCCURENCE_LIMIT:
            tagsOtherDays.add(tagname)
    for tagname in tagsOtherDays:
        for goodDayTagNames in tagsGoodScoreDays:
            if tagname in goodDayTagNames:
                existingToPrint[tagname] = existingToPrint.get(tagname,0)
                existingToPrint[tagname]+=1
            else:
                nonExistingToPrint[tagname] = nonExistingToPrint.get(tagname,0)
                nonExistingToPrint[tagname]+=1
    nr_of_good_days = len(tagsGoodScoreDays)    
    freqGroups = [[],[],[],[],[],[],[],[]] #90+, 80+, 70+, 60+, 50+, 40+, 30+, 20+    
    nonFreqGroups = [[],[],[],[],[],[],[],[]] #90+, 80+, 70+, 60+, 50+, 40+, 30+, 20+      
    for tagname in sorted (existingToPrint, key=existingToPrint.get, reverse=True):
        percentage = existingToPrint[tagname]/nr_of_good_days
        if percentage >= 0.9:
            freqGroups[0].append(tagname)
        elif percentage >= 0.8: 
            freqGroups[1].append(tagname)
        elif percentage >= 0.7: 
            freqGroups[2].append(tagname)
        elif percentage >= 0.6: 
            freqGroups[3].append(tagname)
        elif percentage >= 0.5: 
            freqGroups[4].append(tagname)
        elif percentage >= 0.4: 
            freqGroups[5].append(tagname)
        elif percentage >= 0.3: 
            freqGroups[6].append(tagname)
    for tagname in sorted (nonExistingToPrint, key=nonExistingToPrint.get, reverse=True):
        percentage = nonExistingToPrint[tagname]/nr_of_good_days
        if percentage >= 0.9:
            nonFreqGroups[0].append(tagname)
        elif percentage >= 0.8: 
            nonFreqGroups[1].append(tagname)
        elif percentage >= 0.7: 
            nonFreqGroups[2].append(tagname)
        elif percentage >= 0.6: 
            nonFreqGroups[3].append(tagname)
        elif percentage >= 0.5: 
            nonFreqGroups[4].append(tagname)
        elif percentage >= 0.4: 
            nonFreqGroups[5].append(tagname)
        elif percentage >= 0.3: 
            nonFreqGroups[6].append(tagname)
    f.write('Nr of days(>=Score'+str(min_score)+'): '+str(nr_of_good_days)+'\n\n')        
    f.write('90% of these days contains tags:\n')
    for tagname in freqGroups[0]:
        f.write(tagname +' ')
    f.write('\n80%:\n')
    for tagname in freqGroups[1]:
        f.write(tagname +' ')
    f.write('\n70%:\n')
    for tagname in freqGroups[2]:
        f.write(tagname +' ')
    f.write('\n60%:\n')
    for tagname in freqGroups[3]:
        f.write(tagname +' ')
    f.write('\n50%:\n')
    for tagname in freqGroups[4]:
        f.write(tagname +' ')
    f.write('\n40%:\n')
    for tagname in freqGroups[5]:
        f.write(tagname +' ')
    f.write('\n30%:\n')
    for tagname in freqGroups[6]:
        f.write(tagname +' ')
        
    f.write('\n\n90% of these days do NOT contains tags:\n')
    for tagname in nonFreqGroups[0]:
        f.write(tagname +' ')
    f.write('\n80%:\n')
    for tagname in nonFreqGroups[1]:
        f.write(tagname +' ')