# -*- coding: utf-8 -*-
"""
Created on Sun May 22 08:48:36 2016

@author: Johan

Att tänka på: 
1. floats, . eller ,. Hardcodar jag det här kommer det bara att fungera för ,. Bästa är att använda [.,] => enkelt
2. Unicode. <Ströss> ska vara lika ok som <Stress>
3. def parseToDays => Jag sätter nu en rowMax i absoluta tal. Givetvis ej ok. SKa vara rowMax = ws.max_row. Fixa det som inte fungerar med det.

"""
from datetime import datetime
from openpyxl import load_workbook
import re
from main_classes import Day, Meal, BM, Exercise, Condition, Other, Tag


#ExcelImporter
#Vi startar med de tre översta funktionerna (vi hämtar dagarna innan dagarna med viss poäng och skriver sen ut dessa dagar)
def parseToDays(filename):
    
    wb = load_workbook(filename = filename)
    # grab the active worksheet
    ws = wb.active
    #rowMax = ws.max_row
    rowMax = 78    #Måste vara (tack vare att jag räknar på score i nästföljande dag i modulen analyzer) en rad mindre än fullföljd logg
    #rowMax = ws.max_row    
    colMax = ws.max_column
    days = []
    
    for row in range(3,rowMax):
        day = []
        meals = []
        bms = []
        exercises = []
        conditions = []
        others = []
        
        cDate =  ws.cell(row=row, column = 1)
        cScore = ws.cell(row=row, column = 2) 
        i = 0        
        for col in range(4,colMax):   
            cValue = ws.cell(row=row, column=col)      
            s = cValue.value 
            if s!=None:
                s_lower = s.lower() 
                if s_lower.startswith('<ignore>'):
                    continue
                elif s_lower.startswith('<meal>'):
                    i+=1                    
                    meals.append(strToMeal(s))
                elif s_lower.startswith('<bm>'):
                    bms.append(strToBM(s))
                elif s_lower.startswith('<exercise>'):
                    exercises.append(strToExercise(s))    
                elif s_lower.startswith('<condition>'):
                    conditions.append(strToCondition(s))    
                elif s_lower.startswith('<other>'):
                    others.append(strToOther(s))  
        day = Day(cDate.value,cScore.value, meals, bms, exercises,conditions, others)
        days.append(day) 
    return days          
#arg: '<Meal><Time>11:00<Tags> [#tonfisk #ris #olivolja #oliver #tomater #green_leaves]*2 #tonfisk<Size>2.7/5<Comment>Dubbel portion lunch'   
def strToMeal(s):
    time = strToTime(s)
    tags = strToTags(s)
    portions = strToPortions(s)
    return Meal(time, tags, portions)

#arg: '<BM><Time>15:00<Tags>#complete<Size>2,5/5 <Bristol>4 <Comment>...'
def strToBM(s):
    time = strToTime(s)
    tags = strToTags(s)
    size = strToSize(s)
    bristol = strToBristol(s)
    return BM(time, tags, size, bristol) 
#arg: '<Exercise><Time>17:00<Tags>#löpning <Intensity> 5/5<Comment>Sprang på  27:37. Bagarmossenrundan'
def strToExercise(s):
    time = strToTime(s)
    tags = strToTags(s)
    intensity = strToIntensity(s)
    return Exercise(time, tags, intensity)    
#arg: '<Condition><Time>10:00 <Tags>#fräsch<Comment>Varit på jobbet'
def strToCondition(s):
    time = strToTime(s)
    tags = strToTags(s)
    return Condition(time, tags)  
#arg: '<Other><Time>12:00<Tags>#APC_pet*1/2<Comment>Borde'
def strToOther(s):
    time = strToTime(s)
    tags = strToTags(s)
    return Other(time, tags)   
    
#arg: '09:15' or '9:15'
def strToTime(s):
    #timeStr = re.search('\d{1,2}:\d\d',s).group()  #ska bara vara denna sträng ovan return-satsen. Och exceptions
    timeStr = '00:00'       #remove - only for testing!
    s = re.search('\d{1,2}:\d\d',s)
    if s is not None:
        timeStr = s.group()
    return datetime.strptime(timeStr,"%H:%M").time()
#from datetime import datetime
def strToPortions(s):    
    pat = re.compile("""
    (?<=<Portions>)         #lookbehind
    \s*?                 #doesnt crash for spaces
    \d[.]?\d?            #one number, optionable dot, one number (only if dot)
    """,re.VERBOSE)
    
    sizeStr =   re.search(pat,s).group()  
    return float(sizeStr)
#very similar to strToPortions
def strToSize(s):    
    pat = re.compile("""
    (?<=<Size>)         #lookbehind
    \s*                 #doesnt crash for spaces
    \d[.]?\d?            #one number, optionable dot, one number (only if dot)
    """,re.VERBOSE)
    sizeStr = ''    
    try:    
        sizeStr =   re.search(pat,s).group()
    except: #For example, if no size available
        sizeStr='2'
    return float(sizeStr)
def strToBristol(s):
    pat = re.compile("""
    (?<=<Bristol>)      #lookbehind
    \s*                 #spaces irrevelant
    \d?                  #one integer
    """,re.VERBOSE)
    bristolStr =   re.search(pat,s).group()
    return int(bristolStr)  
def strToIntensity(s):
    pat = re.compile("""
    (?<=<Intensity>)         #lookbehind
    \s*                 #doesnt crash for spaces
    \d[.]?\d?            #one number, optionable dot, one number (only if dot)
    """,re.VERBOSE)
    intenStr =   re.search(pat,s).group()
    return float(intenStr)    

#accepts strings like '<Tags>[#sdfds #dsfdsf]*2'   => there MUST be whitespace between tags (doesn't matter how many whitespaces). String can end with whitespace  
#returns array of Tag objects
def strToTags(s):
    #remove '<Tags>'
    s = s.replace('<Tags>','')  
    
    #trim string   
    s = s.strip()
    
    #if empty string
    if len(s)==0:
        return [] 
    return parseStrOfTags(s)

#arg: string of the form '[#milk #honey]*2 #sugar'
#returns: array with tags 
def parseStrOfTags(s):
    tags = []    
    tokenizedTags = []    
    
    #Many of the questionmarks in regex are just there to make it work (it seems then to search consistently left to right then, and not jump immidiately to the end and search backwards, which makes it fail in case there are several tagBundles with brackets), probably not the best regex...
    #regex = '\[.*?\]\*\d+,?\d?|#.*? '
    #regex = '(\[.*?\]\*\d+(.\d)?)|(#.*? )'
    pat = re.compile(r"""
    \[.+?\]              #between brackets
    \*\d+?               #brackets multiplied with...
    [.,]?\d?             #optional dot (comma also ok) and then one optional digit
    |                    #OR...
    \#[\w@]+             #ordinary hashtag (must end with space) Be sure to include both \w AND @ (the latter divides a tag into two or more related tags)
    """,re.VERBOSE|re.UNICODE)    
    
    tagBundleArray = re.findall(pat,s)
    for tagBundle in tagBundleArray:
        tagBundle.strip()
        if tagBundle[0]=='#':
            tokenizedTags.append(tagBundle) #remove #
        elif tagBundle[0]=='[':
            tagStrings = parseTagsInsideBracket(tagBundle)
            tokenizedTags.extend(tagStrings)
        else:
            print ('error should be thrown')
    
   # print 'tokenizedTags: '+tokenizedTags[0]    
    for strTag in tokenizedTags:
        tagz = strToTagz(strTag[1:]) #remove #
        tags.extend(tagz)    
    return tags

#arg: string of the form '[#milk*3.3 #pizza@wheat*2,2]*2'
#returns array with strings: ['#milk*6.6', '#pizza@wheat*4.4'] => n.b. => commas turns into dots

def parseTagsInsideBracket(tagBundle):  
    multipliedTagStrings = []    
    #get multiplier as string(inclusive of *) ex => '*2,5'
    pat =    re.compile(r"""
    (?<=\]\*)             #lookbehind assertion => start looking after ]*
    \d+?                  #brackets multiplied with digit/-s
    ([.,]\d)?                #optional dot (or comma) and then one optional digit
    """,re.VERBOSE|re.UNICODE)          
    multiplier = re.search(pat,tagBundle).group()   #get 2.0   
    
    tagBundle = tagBundle[1:tagBundle.index(']')]   #slice [#milk*3.3 #pizza@wheat*2,2]*2 => #milk #pizza@wheat, *2    
    tagStrings = re.split('\s+',tagBundle)          #split #milk*3.3 #pizza@wheat*2,2 => #milk*3.3, #pizza@wheat*2,2  

    #multiply every tag with multiplier     
    for tagString in tagStrings: 
        multipliedTagStrings.append(addMultToTag(tagString,multiplier))  
    return multipliedTagStrings
    
#arg: tagString -> #pizza@wheat*2,2 OR #okra, mult -> 3.2 
def addMultToTag(tagString, mult):
    #, -> .    
    tagString = tagString.replace(',','.')    
    mult = mult.replace(',','.')  
    
    
    strAndMult = tagString.split('*')
    if len(strAndMult)==1:  #okra
        tagString = tagString +'*'+ mult
    elif len(strAndMult)==2:
       multiMult = float(strAndMult[1])*float(mult)
       tagString = tagString + '*' + str(multiMult)
    else:
        print('Exception should be thrown for addMultToTag in excel_importer')
    return tagString
#arg s should look something like: pizza@cheese@wheat*2.2 => [Tag(name=spinach, portion=2.2), Tag(name=green_leaves, portion=2)]
def strToTagz(s):
    tags = []    
    portion = 1.0        
   
    #trim string   
    s = s.strip()
    
    #get portion
    namesAndMult = s.split('*')
    if len(namesAndMult)==2:                    
        portion = float(namesAndMult[1].replace(',','.'))
    for name in namesAndMult[0].split('@'): 
        tags.append(Tag(name,portion))
    return tags   