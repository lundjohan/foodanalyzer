# -*- coding: utf-8 -*-
"""
Created on Sat May 28 20:28:03 2016

@author: Johan
"""
from analyzer import getRec, getMealsStat, blueZonesStat, goodZonesStat, ordinaryZonesStat, darkZonesStat, moveUpFollowingDayPoints, timeLastMeal, removeDaysWithoutTags, getWithoutTag
from excel_importer import parseToDays

def printToFiles(filename):
    allDays = parseToDays(filename)
    handleDaysByScore(r'stats\blueZones.txt', allDays, 'blue')
    handleDaysByScore(r'stats\darkZones.txt', allDays, 'dark')
    handleDaysByScore(r'stats\goodZones.txt', allDays, 'good')
    handleDaysByScore(r'stats\ordinaryZones.txt', allDays, 'ordinary')
    handleLastMeal(r'stats\timeOfLastMeal.txt', allDays)
    handleNrOfMeals(r'stats\nrOfMeals.txt', allDays)    
    handlePortions(r'stats\portionsSizes.txt', allDays)
    handleRecommendations(r'stats\tags_average_day_score.txt', allDays)
    handleDaysWithoutTag(r'stats\average_score_without_tag.txt', allDays)
def handleLastMeal(fileName, allDays):
    descr = 'Print the average Day Score(divided on same day, and days score following lastMeal) for lastMeal'
    baseHandler(fileName,descr,allDays,timeLastMeal)  
def handleDaysByScore(fileName, allDays, score):
    descr = 'Similiarities of days with certain score intervals'    
    if score == 'blue':    
       baseHandler(fileName,descr,allDays,blueZonesStat) 
    elif score =='good':
       baseHandler(fileName,descr,allDays,goodZonesStat)
    elif score =='ordinary':
       baseHandler(fileName,descr,allDays,ordinaryZonesStat)
    elif score =='dark':
       baseHandler(fileName,descr,allDays,darkZonesStat) 
     
    
def handlePortions(fileName, allDays):
    descr = 'Correlation between nr of portions (e.g. nr of meals/day * mealSizes) and score'
    baseHandler(fileName,descr,allDays,getMealsStat)
def handleNrOfMeals(fileName, allDays):
    descr = 'Correlation between nr of meals and score'
    baseHandler(fileName,descr,allDays,getMealsStat)
def handleRecommendations(fileName, allDays):
    descr = 'Print the average Day Score(divided on same day, and days score following tag) for tag'
    baseHandler(fileName,descr,allDays,getRec)       
def handleDaysWithoutTag(fileName, allDays):
    descr = 'Days WITHOUT tag => a high score might indicate that it is good to avoid tag'
    baseHandler(fileName,descr,allDays,getWithoutTag)  
def baseHandler(fileName, descr,allDays, methodToRun ):
    with  open(fileName,'w') as f:
        f.write('DESCRIPTION\n'+descr)
        f.write("\n\nBASED ON SAME DAY'S POINTS:\n")
        methodToRun(f, removeDaysWithoutTags(allDays))
        f.write("\n\nBASED ON FOLLOWING DAY'S POINTS:\n")
        pointsMovedUp =  moveUpFollowingDayPoints(removeDaysWithoutTags(allDays))       
        methodToRun(f, pointsMovedUp)
        f.write("\n\nBASED ON POINTS BELONGING TO DAY AFTER TOMORROW:\n")
        pointsMovedUpTwoDays =  moveUpFollowingDayPoints(removeDaysWithoutTags(pointsMovedUp))
        methodToRun(f, pointsMovedUpTwoDays)