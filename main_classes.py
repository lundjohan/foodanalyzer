# -*- coding: utf-8 -*-
"""
Created on Sun May 22 08:49:55 2016

@author: Johan
"""
from datetime import datetime
AFTER_BREAKFAST = datetime.strptime('10:00',"%H:%M").time()
class Day:
    """Day class"""
    #data attributes
    def __init__(self, date, score, meals, bms, exercises,conditions, others):
        self.date = date
        self.score = score
        self.meals = meals
        self.bms = bms
        self.exercises = exercises
        self.conditions = conditions
        self.others = others
    def tags(self):
        tags = []
        for meal in self.meals:            
            tags.extend(meal.tags)
        for bm in self.bms:            
            tags.extend(bm.tags)
        for exercise in self.exercises:            
            tags.extend(exercise.tags)
        for condition in self.conditions:            
            tags.extend(condition.tags)
        for other in self.others:            
            tags.extend(other.tags)
        return tags
    def tagsExtended(self):
        tags = self.tags()
        largestPortionDay = 0 
        nrOfPortionsDay = 0
        
        #Did I exercise this day?
        if len(self.exercises)>0:
            tags.append(Tag('<Did Exercise>',1))
            
            intense = False
            
            for e in self.exercises:
                if e.intensity >= 4:
                    intense = True
            if intense:
               tags.append(Tag('<Did Exercise >= 4>',1)) 
        #How big breakfast?        
        if len(self.meals)>0 and self.meals[0].time < AFTER_BREAKFAST:
            if self.meals[0].portions == 2:
                tags.append(Tag('<Breakfast portion == 2>',1))        
            elif self.meals[0].portions == 1:
                tags.append(Tag('<Breakfast portion == 1>',1))
           # elif self.meals[0].portions == 1:
            #    tags.append(Tag('Breakfast portion == 1',1))
                
        for meal in self.meals:
            nrOfPortionsDay += meal.portions
            if meal.portions>largestPortionDay:
                largestPortionDay = meal.portions
        if largestPortionDay > 3.5:
            tags.append(Tag('3.5<largest_portion'))
        elif largestPortionDay > 2.5:
            tags.append(Tag('2.5<largest_portion<=3.5'))
        elif largestPortionDay > 1.5:
            tags.append(Tag('\'1.5<largest_portion<=2.5\''))
        elif largestPortionDay <=1.5 and largestPortionDay >=0.0:
            tags.append(Tag('0<largest_portion<=1.5'))
        tagName =''    
        if nrOfPortionsDay> 8:
            tagName = '8<portions'
        elif nrOfPortionsDay> 6:
            tagName = '6<portions<=8'
        elif nrOfPortionsDay> 4:
            tagName = '4<portions<=6'
        elif nrOfPortionsDay> 0 and nrOfPortionsDay<=4:
            tagName = '0<portions<=4'
        tags.append(Tag(tagName,1))
        
        #nr of bms        
        tags.append(Tag('<'+str(len(self.bms))+' nr of bms>'))
        return tags
    def inputTags(self):
        tags = []
        for meal in self.meals:            
            tags.extend(meal.tags)
        for exercise in self.exercises:            
            tags.extend(exercise.tags)
        for other in self.others:            
            tags.extend(other.tags)
        return tags
    def tagNames(self):
        tagNames = set()
        for tag in self.tagsExtended():
            tagNames.add(tag.name)
        return tagNames
        
class Meal:
    def __init__(self, time, tags, portions):
         self.time = time
         self.tags = tags
         self.portions = portions
class BM:
    def __init__(self, time, tags, size, bm):
         self.time = time
         self.tags = tags
         self.size = size
         self.bm = bm    
class Exercise:
    def __init__(self, time, tags, intensity):
         self.time = time
         self.tags = tags
         self.intensity = intensity
class Condition:
    def __init__(self, time, tags):
         self.time = time
         self.tags = tags 
class Other:
    def __init__(self, time, tags):
         self.time = time
         self.tags = tags          
#write for example spinach@green_leaves => 2 tags: spinach & green_leaves
class Tag:
    def __init__(self, name, portion = 1):
        self.name= name       
        self.portion = portion